<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Form\ReservationType;
use Doctrine\Migrations\Configuration\EntityManager\ManagerRegistryEntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/home", name="home")
     */
    public function index(): Response
    {


        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'test'=> 'test2'
        ]);
    }
    /**
     * Undocumented function
     *
     * @return Response
     * @Route("/show", name="show_room")
     */
    public function showRoom(): Response
    {
        return $this->render('home/showRoom.html.twig');
    }


    /**
     * @Route("/detail", name="deatil_room")
     *
     * @return Response
     */
    public function detailRoom(Request $request): Response
    {
        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if(!$reservation->getId()){
                $this->entityManager->persist($reservation);
            }
            $this->entityManager->flush();
        }

        return $this->render('home/datailRoom.html.twig',[
            'form' => $form->createView(),
            'all_reserve'=>$this->getDoctrine()->getRepository(Reservation::class)->findAll()
        ]);
    }
}
