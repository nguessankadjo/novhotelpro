<?php

namespace App\Entity;

use App\Repository\ReservationRoomRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRoomRepository::class)
 */
class ReservationRoom
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $arriver;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $depart;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $inviteRoom;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paiement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArriver(): ?string
    {
        return $this->arriver;
    }

    public function setArriver(string $arriver): self
    {
        $this->arriver = $arriver;

        return $this;
    }

    public function getDepart(): ?string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getInviteRoom(): ?string
    {
        return $this->inviteRoom;
    }

    public function setInviteRoom(string $inviteRoom): self
    {
        $this->inviteRoom = $inviteRoom;

        return $this;
    }

    public function getPaiement(): ?bool
    {
        return $this->paiement;
    }

    public function setPaiement(bool $paiement): self
    {
        $this->paiement = $paiement;

        return $this;
    }
}
