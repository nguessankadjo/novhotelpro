<?php

namespace App\Entity;

use App\Repository\CommandeRoomRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRoomRepository::class)
 */
class CommandeRoom
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $arrive_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArriveDate(): ?string
    {
        return $this->arrive_date;
    }

    public function setArriveDate(string $arrive_date): self
    {
        $this->arrive_date = $arrive_date;

        return $this;
    }
}
