<?php

namespace App\Repository;

use App\Entity\CommandeRoom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CommandeRoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommandeRoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommandeRoom[]    findAll()
 * @method CommandeRoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandeRoom::class);
    }

    // /**
    //  * @return CommandeRoom[] Returns an array of CommandeRoom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommandeRoom
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
